import math
from typing import Tuple


def distance(pos: Tuple[int, int], other_pos: Tuple[int, int]) -> float:
    return math.sqrt((pos[0] - other_pos[0]) ** 2 + (pos[1] - other_pos[1]) ** 2)
