from dataclasses import dataclass


@dataclass
class AgentConfig:
    max_energy: int  # how much energy can store organism
    max_age: int  # after how many steps organism dies
    energy_step: float  # how many energy organism consumes each step
    reproduction_chance: float  # prolific organism has a chance of reproduction each step, which decreases with age
    reproduction_age: int  # after this many steps organism can reproduce


class Config:
    REPRODUCTION_ENERGY_THRESHOLD = 50
    ENERGY_CONVERSION_RATE = 0.1
    GRASS = AgentConfig(
        max_energy=100,
        max_age=25,
        energy_step=0.3,
        reproduction_chance=0.4,
        reproduction_age=2
    )
    HARE = AgentConfig(
        max_energy=250,
        max_age=70,
        energy_step=0.9,
        reproduction_chance=0.6,
        reproduction_age=13
    )
    FOX = AgentConfig(
        max_energy=400,
        max_age=200,
        energy_step=0.6,
        reproduction_chance=0.3,
        reproduction_age=16
    )
