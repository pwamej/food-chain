# Food Chain Multi-Agent Simulation

## Local setup

(requires [pipenv](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv))

Set up virtual environment and Install dependencies:
```bash
pipenv install --dev
```

Activate virtual environment:
```bash
pipenv shell
```

Run visualization:
```bash
mesa runserver
```

