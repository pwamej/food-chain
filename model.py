from typing import Tuple

from mesa import Agent, Model
from mesa.datacollection import DataCollector
from mesa.space import MultiGrid
from mesa.time import RandomActivation

from agents.fox import Fox
from agents.grass import Grass
from agents.hare import Hare
from config import Config


class FoodChainModel(Model):
    def __init__(self, plant_n, hare_n, fox_n, width, height):
        super().__init__()
        self.running = True
        self.schedule = RandomActivation(self)
        self.grid = MultiGrid(width, height, True)
        self.datacollector = DataCollector(
            model_reporters={
                "Grass energy": self.population_energy(Grass),
                "Hare energy": self.population_energy(Hare),
                "Fox energy": self.population_energy(Fox),
                "Grass count": self.population_count(Grass),
                "Hare count": self.population_count(Hare),
                "Fox count": self.population_count(Fox)
            })
        for i in range(plant_n):
            self.add_agent(Grass(self, Config.GRASS.max_energy / 3))

        for i in range(hare_n):
            self.add_agent(Hare(self, Config.HARE.max_energy / 3))

        for i in range(fox_n):
            self.add_agent(Fox(self, Config.FOX.max_energy / 3))

    def step(self):
        self.datacollector.collect(self)
        self.schedule.step()
        if any(self.population_energy(x)(self) <= 0 for x in [Grass, Hare, Fox]):
            self.running = False

    def add_agent(self, a: Agent, pos: Tuple[int, int] = None):
        """
        Add agent to schedule and place it on grid.
        :param a: agent to be added
        :param pos: position on grid where agent should be added, selected at random if not provided
        """
        self.schedule.add(a)
        if pos is None:
            x = self.random.randrange(self.grid.width)
            y = self.random.randrange(self.grid.height)
            pos = (x, y)
        self.grid.place_agent(a, pos)

    @staticmethod
    def population_energy(species):
        return lambda model: sum(agent.energy for agent in model.schedule.agents if isinstance(agent, species))

    @staticmethod
    def population_count(species):
        return lambda model: sum(1 for agent in model.schedule.agents if isinstance(agent, species))
