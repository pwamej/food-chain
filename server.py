from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter
from mesa.visualization.modules import CanvasGrid, ChartModule

from agents.fox import Fox
from agents.grass import Grass
from agents.hare import Hare
from model import FoodChainModel

GRID_SIZE = 50


def agent_portrayal(agent):
    if type(agent) is Grass:
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Color": "green",
                     "Layer": 0,
                     "r": 0.3}
    else:
        color = {
            Hare: "gray",
            Fox: "orange"
        }
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Color": color[type(agent)],
                     "Layer": 1,
                     "r": 0.5}
    return portrayal


grid = CanvasGrid(agent_portrayal, GRID_SIZE, GRID_SIZE, 500, 500)

energy_chart = ChartModule([{"Label": "Grass energy",
                             "Color": "Green"},
                            {"Label": "Hare energy",
                             "Color": "Gray"},
                            {"Label": "Fox energy",
                             "Color": "Orange"},
                            {"Label": "Grass count",
                             "Color": "Green"},
                            {"Label": "Hare count",
                             "Color": "Gray"},
                            {"Label": "Fox count",
                             "Color": "Orange"}
                            ],
                           data_collector_name='datacollector')


plant_n_slider = UserSettableParameter('slider', "Initial plant count", 420, 1, 500, 1)

hare_n_slider = UserSettableParameter('slider', "Initial hare count", 150, 1, 500, 1)

fox_n_slider = UserSettableParameter('slider', "Initial fox count", 10, 1, 500, 1)


server = ModularServer(FoodChainModel,
                       [grid, energy_chart],
                       "Food chain",
                       {"plant_n": plant_n_slider,
                        "hare_n": hare_n_slider,
                        "fox_n": fox_n_slider,
                        "width": GRID_SIZE,
                        "height": GRID_SIZE})

server.port = 8521
