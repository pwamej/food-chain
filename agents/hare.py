from agents.animal import Animal
from agents.grass import Grass
from config import Config


class Hare(Animal):
    def __init__(self, model, energy: int):
        super().__init__(model, Grass, Config.HARE, energy)
