from agents.organism import Organism
from config import Config

REPRODUCTION_DISTANCE = 7
REPRODUCTION_ITERATIONS = 1

ENERGY_FROM_SUN = 25


class Grass(Organism):
    def __init__(self, model, energy: int):
        super().__init__(model, Config.GRASS, energy)

    def step(self):
        super().step()
        if self.alive:
            self._produce()

    def _produce(self):
        if self.energy < self.config.max_energy:
            # bonus_energy = len(self._free_fields_nearby()) * self.config.energy_step
            self.energy += ENERGY_FROM_SUN

    def _reproduce(self):
        for x in range(REPRODUCTION_ITERATIONS):
            free_fields = self._free_fields_nearby(REPRODUCTION_DISTANCE)
            # if free_fields and len(free_fields) > REPRODUCTION_DISTANCE * REPRODUCTION_DISTANCE * 0.86:
            if free_fields:
                offspring_pos = self.random.choice(free_fields)
                self.model.add_agent(Grass(self.model, self.energy / 2), offspring_pos)
