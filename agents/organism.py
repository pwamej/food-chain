import uuid
from abc import ABCMeta, abstractmethod
from typing import Tuple, List

from mesa import Agent

from config import AgentConfig
from utils import distance


class Organism(Agent, metaclass=ABCMeta):
    def __init__(self, model, config: AgentConfig, energy: int):
        super().__init__(uuid.uuid4(), model)
        self.energy = energy
        self.age = 0
        self.alive = True
        self.config = config

    def step(self):
        self._live()
        if self.alive and self.config.reproduction_age < self.age < self.config.max_age:
            reproduction_chance = self.config.reproduction_chance * (1 + 1 / (self.age - self.config.max_age))
            if self.random.random() <= reproduction_chance:
                self._reproduce()
                self.energy *= (2 / 3)

    def die(self):
        self.model.schedule.remove(self)
        self.model.grid.remove_agent(self)
        self.alive = False

    def distance(self, other_pos: Tuple[int, int]) -> float:
        return distance(self.pos, other_pos)

    def _live(self):
        self.energy -= self.config.energy_step
        self.age += 1
        if self.energy <= 0 or self.age == self.config.max_age:
            self.die()

    def _free_fields_nearby(self, radius=1) -> List[Tuple[int, int]]:
        """
        Returns list of fields in neighborhood whose don't contain organism of same species.
        :param radius: radius of neighborhood
        :return: list of positions
        """
        def contains_species(pos: Tuple[int, int]):
            return any(isinstance(x, type(self)) for x in self.model.grid.get_cell_list_contents(pos))

        fields_nearby = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False, radius=radius)
        return [x for x in fields_nearby if not contains_species(x)]

    def _fields_with_same_specie(self, radius=1) -> List[Tuple[int, int]]:
        """
        Returns list of fields in neighborhood whose don't contain organism of same species.
        :param radius: radius of neighborhood
        :return: list of positions
        """

        def contains_species(pos: Tuple[int, int]):
            return any(isinstance(x, type(self)) for x in self.model.grid.get_cell_list_contents(pos))

        fields_nearby = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=False, radius=radius)
        return [x for x in fields_nearby if contains_species(x)]

    @abstractmethod
    def _reproduce(self):
        pass
