from agents.animal import Animal
from agents.hare import Hare
from config import Config


class Fox(Animal):
    def __init__(self, model, energy: int):
        super().__init__(model, Hare, Config.FOX, energy)
        self.scan_distance = 6
        self.reproduce_distance = 7

