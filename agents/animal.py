from typing import Type, List

from agents.grass import Grass
from agents.organism import Organism
from config import Config, AgentConfig
from utils import distance


class Animal(Organism):
    def __init__(self, model, food_type: Type[Organism], config: AgentConfig, energy: int):
        super().__init__(model, config, energy)
        self.scan_distance = 2
        self.reproduce_distance = 3
        self.food_type = food_type

    def step(self):
        super().step()
        if self.alive:
            self._eat()
            self._move()

    def _move(self):
        possible_moves = self.model.grid.get_neighborhood(self.pos, moore=True, include_center=True)
        agents_nearby = self.model.grid.get_neighbors(self.pos, True, True, self.scan_distance)
        food_nearby = [x.pos for x in agents_nearby if isinstance(x, self.food_type)]
        if food_nearby:
            nearest_food = min(food_nearby, key=self.distance)
            new_position = min(possible_moves, key=lambda x: distance(x, nearest_food))
        else:
            new_position = self.random.choice(possible_moves)
        self.model.grid.move_agent(self, new_position)

    def _eat(self):
        """
        Consumes random nearby food.
        """
        food_list = self._food_nearby()
        if food_list and self.energy < self.config.max_energy - 3 * self.config.energy_step:
            food = self.random.choice(food_list)
            food.die()
            self.energy += Config.ENERGY_CONVERSION_RATE * food.energy

    def _reproduce(self):
        free_fields = self._free_fields_nearby()
        if free_fields and self._fields_with_same_specie(self.reproduce_distance).__len__() > 1:
            offspring_pos = self.random.choice(free_fields)
            self.model.add_agent(type(self)(self.model, self.energy / 6), offspring_pos)

    def _food_nearby(self) -> List[Organism]:
        agents_nearby = self.model.grid.get_neighbors(self.pos, moore=True, include_center=True)
        return [x for x in agents_nearby if self._is_food(x) and x.alive]

    def _is_food(self, agent):
        return isinstance(agent, self.food_type)

